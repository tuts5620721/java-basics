package enums;

public enum Wochentage {
    MONTAG(1),
    DIENSTAG(2),
    MITTWOCH(3),
    DONNERSTAG(4),
    FREITAG(5),
    SAMSTAG(6),
    SONNTAG(7);

    final int kennNrTag;

    Wochentage (int kennNrTag){
        this.kennNrTag = kennNrTag;
    }

}
