package lambdas;

public class CalculatorImpl {

/*    @Override
    public void switchOn(){System.out.println("Switch ON !");
    }*/

    public static void main(String[] args) {
/*
        Calculator calculator = () -> System.out.println("Switched ON !");
        calculator.switchOn();
*/

/*        Calculator calculator = (input) -> System.out.println("Input: "+input);
        calculator.sum(16);*/

/*        Calculator calculator= (i1, i2 ) -> {
            return i1 - i2;
        };*/
        //geht noch kürzer
/*        Calculator calculator= (i1, i2 ) -> i1 - i2;

        System.out.println(calculator.subtract(10,7));*/
        Calculator calculator = (a,b) -> a * b;
        System.out.println(calculator.multiply(4,7));
    }
}
//  () -> {body};

