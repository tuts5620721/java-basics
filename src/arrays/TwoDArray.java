package arrays;

import java.util.Arrays;

public class TwoDArray {

    public static void getArray() {
        String myArray[][] = new String[2][3];

        int count = 1;
        for (int r = 0; r < myArray.length; r++) {
            myArray[r] = new String[]{"g"};
            System.out.println(Arrays.deepToString(myArray[r]));
            for (int c = 0; c < myArray.length; c++) {
                myArray[c] = new String[]{"P"};
                System.out.println(Arrays.deepToString(myArray[c]));
            }
        }
    }
    public static void getSecondArr() {
        int [][] secArr = new int[3][3];
        int count = 1;
        for (int r=0; r < secArr.length; r++){
            for (int c=0; c < secArr[r].length; c++)
                secArr[r][c] = count++;
            System.out.println(secArr);
        }
        System.out.println(secArr);
    }
}
