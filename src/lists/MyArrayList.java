package lists;

import java.util.ArrayList;

public class MyArrayList {



    public void getArr(){
        //simples Array kannst Primitive und Objekte reinpacken
        int[] nums = new int[10];
        nums[0] = 1;
        nums[1] = 2;
        nums[2] = 3;
        nums[3] = 4;

        System.out.println(nums);

    }

    public void getArrLis(){
        ArrayList<String> students = new ArrayList<>();
        students.add("hansi");
        students.add("klaus");
        students.add("Max");
        students.add("Johann");
        students.add("Jack");
        System.out.println(students);

        System.out.println("Größe von Students ArrList: " +students.size());
        System.out.println("Länge von Students Arrlist: " +students.toArray().length);

        if (students.size() <= 4){
            System.out.println("Sie haben die mind zahl von 5 Studenten nicht erreich !");
        }
        else {
            System.out.println("Mindestanzahl an Studs erreicht !");
        }

    }

    /*public void getArrLisSecond(){
        getArrLis();
        System.out.println(students);
    }*/



}
